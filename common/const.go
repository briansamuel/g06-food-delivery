package common

const (
	DbTypeRestaurant = 1
	DbTypeUser       = 2
	DbTypeFood       = 3
	DbTypeCategory   = 4
)

const (
	CurrentUser = "user"
)

const (
	TopicUserLikeRestaurant    = "user.like.restaurant"
	TopicUserDislikeRestaurant = "user.dislike.restaurant"
	TopicUserUpdateLocation    = "user.update.location"
)

type Requester interface {
	GetUserId() int
	GetEmail() string
	GetRole() string
}
