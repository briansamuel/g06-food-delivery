package appctx

import (
	"g06-food-delivery/common"
	uploadbusiness "g06-food-delivery/modules/upload/business"
	"g06-food-delivery/pubsub"
	"g06-food-delivery/skio"
	socketio "github.com/googollee/go-socket.io"
	"gorm.io/gorm"
)

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	UploadProvider() uploadbusiness.UploadProvider
	SecretKey() string
	GetPubSub() pubsub.PubSub
	GetRealtimeEngine() skio.RealtimeEngine
	GetUser(conn socketio.Conn) common.Requester
}

type appCtx struct {
	db             *gorm.DB
	uploadProvider uploadbusiness.UploadProvider
	secretKey      string
	ps             pubsub.PubSub
	rtEngine       skio.RealtimeEngine
}

func NewAppContext(db *gorm.DB, uploadProvider uploadbusiness.UploadProvider, secretKey string, ps pubsub.PubSub, rtEngine skio.RealtimeEngine) *appCtx {
	return &appCtx{db: db, uploadProvider: uploadProvider, secretKey: secretKey, ps: ps, rtEngine: rtEngine}
}

func (ctx *appCtx) GetMainDBConnection() *gorm.DB { return ctx.db }

func (ctx *appCtx) UploadProvider() uploadbusiness.UploadProvider { return ctx.uploadProvider }

func (ctx *appCtx) SecretKey() string { return ctx.secretKey }

func (ctx *appCtx) GetPubSub() pubsub.PubSub { return ctx.ps }

func (ctx *appCtx) GetRealtimeEngine() skio.RealtimeEngine { return ctx.rtEngine }

func (ctx *appCtx) GetUser(conn socketio.Conn) common.Requester {
	return ctx.GetRealtimeEngine().GetUser(conn)
}
