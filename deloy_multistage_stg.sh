#!/usr/bin/env bash

APP_NAME=food-delivery
DEPLOY_CONNECT=root@159.223.89.183

echo "Docker building..."
docker build -t ${APP_NAME} -f ./Docker-multistage .
echo "Docker saving..."

docker save -o ${APP_NAME}.tar ${APP_NAME}

echo "Deploying..."
scp -o StrictHostKeyChecking=no ./${APP_NAME}.tar ${DEPLOY_CONNECT}:~
ssh -o StrictHostKeyChecking=no ${DEPLOY_CONNECT} 'bash -s' < ./deploy/stg.sh
#
echo "Cleaning..."
rm -f ./${APP_NAME}.tar
#
echo "Done"
