package main

import (
	"g06-food-delivery/component/appctx"
	"g06-food-delivery/component/uploadprovider"
	"g06-food-delivery/memcache"
	"g06-food-delivery/middleware"
	"g06-food-delivery/modules/food/transport/ginfood"
	ginrestaurant "g06-food-delivery/modules/restaurant/transport/gin"
	userlikerestaurantgin "g06-food-delivery/modules/restaurantlike/transport"
	ginupload "g06-food-delivery/modules/upload/transport/gin"
	userstore "g06-food-delivery/modules/user/store"
	"g06-food-delivery/modules/user/transport/ginuser"
	"g06-food-delivery/pubsub/localpb"
	"g06-food-delivery/skio"
	subscriber "g06-food-delivery/subcriber"
	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"
	"github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
	jg "go.opencensus.io/exporter/jaeger"
	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/trace"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"net/http"
	"os"
	"time"
)

func main() {

	formatter := new(log.TextFormatter)
	formatter.ForceColors = true
	formatter.TimestampFormat = "2006-01-02 15:04:05"
	formatter.FullTimestamp = true
	log.SetFormatter(formatter)
	log.SetOutput(colorable.NewColorableStdout())
	// Config Log
	if os.Getenv("GIN_MODE") == "RELEASE" {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.TraceLevel)
	}

	log.Info("Setup Config Log")
	// End Config Log

	// Config Sentry
	sentry.Init(sentry.ClientOptions{
		Dsn: "http://1c00b9b671f3496a85dd91f854a8c43b@localhost:9000/3",
	})
	log.Info("Setup Sentry Config")
	// End Config Sentry

	dsn := os.Getenv("MYSQL_CONNECTION")
	secretKey := os.Getenv("SYSTEM_SECRET")

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	log.Info("Connect Mysql")
	if err != nil {
		log.Fatalln("Error mysql:", err)
	}
	db = db.Debug()

	sqlDB, err := db.DB()

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	sqlDB.SetMaxIdleConns(10)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	sqlDB.SetMaxOpenConns(200)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	sqlDB.SetConnMaxLifetime(time.Hour)

	log.Info("Setup Gin Router")
	r := gin.Default()

	s3Provider := uploadprovider.NewS3Provider(
		os.Getenv("S3BucketName"),
		os.Getenv("S3Region"),
		os.Getenv("S3ApiKey"),
		os.Getenv("S3SecretKey"),
		os.Getenv("S3Domain"))
	pb := localpb.NewPubSub()
	rtEngine := skio.NewRtEngine()
	appCtx := appctx.NewAppContext(db, s3Provider, secretKey, pb, rtEngine)
	rtEngine.Run(appCtx, r)
	subscriber.NewSubscriber(appCtx).Start()

	r.Use(middleware.Recover(appCtx))
	r.Static("/static", "./static")
	r.StaticFile("/demo", "./demo.html")
	authStore := userstore.NewSQLStore(appCtx.GetMainDBConnection())
	userCaching := memcache.NewUserCaching(memcache.NewCaching(), authStore)
	middlewareAuth := middleware.RequireAuth(appCtx, userCaching)
	v1 := r.Group("/v1")
	{
		v1.POST("/upload", ginupload.Upload(appCtx), middlewareAuth)
		restaurants := v1.Group("/restaurants", middlewareAuth)
		{
			restaurants.POST("", ginrestaurant.CreateRestaurant(appCtx))

			restaurants.GET("", ginrestaurant.ListRestaurant(appCtx))

			restaurants.GET("/:id", ginrestaurant.GetRestaurant(appCtx))

			restaurants.PUT("/:id", ginrestaurant.UpdateRestaurant(appCtx))

			restaurants.DELETE("/:id", ginrestaurant.DeleteRestaurant(appCtx))

			restaurants.POST("/:id/like", userlikerestaurantgin.Like(appCtx))
			restaurants.DELETE("/:id/dislike", userlikerestaurantgin.Dislike(appCtx))
			restaurants.GET("/:id/user_like", userlikerestaurantgin.UsersLikeRestaurant(appCtx))
		}
		// Food

		foods := v1.Group("/foods", middlewareAuth)
		{
			foods.POST("", ginfood.CreateFood(appCtx))

			foods.GET("", ginfood.ListFood(appCtx))

			foods.GET("/:id", ginfood.GetFood(appCtx))

			foods.PUT("/:id", ginfood.UpdateFood(appCtx))

			foods.DELETE("/:id", ginfood.DeleteFood(appCtx))

		}
		//	User
		user := v1.Group("/users")
		{
			user.POST("/register", ginuser.Register(appCtx))
			user.POST("/login", ginuser.Login(appCtx))
			user.GET("/profile", middlewareAuth, ginuser.Profile(appCtx))

		}
	}

	// Config Tracing Jaeger
	je, err := jg.NewExporter(jg.Options{
		AgentEndpoint: os.Getenv("JAEGER_AGENT_URL"),
		Process:       jg.Process{ServiceName: "G06-Food-Delivery"}},
	)

	if err != nil {
		log.Println(err)
	}

	trace.RegisterExporter(je)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.ProbabilitySampler(0.2)})

	// End Config Tracing Jaeger
	//startDocketIOServer(r, appCtx)
	if err := http.ListenAndServe(
		":8080",
		&ochttp.Handler{Handler: r},
	); err != nil {
		log.Println(err)
	}
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")

}

//func startDocketIOServer(engine *gin.Engine, appCtx appctx.AppContext) {
//	server := socketio.NewServer(&engineio.Options{
//		Transports: []transport.Transport{websocket.Default},
//	})
//
//	server.OnConnect("/", func(s socketio.Conn) error {
//		fmt.Println("Socket connected:", s.ID(), " IP:", s.RemoteAddr())
//		s.Join("Shipper")
//
//		return nil
//	})
//
//	server.OnEvent("/", "test", func(s socketio.Conn, msg string) {
//		log.Println("test:", msg)
//	})
//
//	server.OnEvent("/", "chat message", func(s socketio.Conn, msg string) {
//		s.Emit("chat message", msg)
//	})
//
//	go server.Serve()
//
//	engine.GET("/socket.io/*any", gin.WrapH(server))
//	engine.POST("/socket.io/*any", gin.WrapH(server))
//}
