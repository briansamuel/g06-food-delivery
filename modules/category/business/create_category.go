package categorybusiness

import (
	"context"
	"g06-food-delivery/common"
	categorymodel "g06-food-delivery/modules/category/model"
	foodmodel "g06-food-delivery/modules/food/model"
)

type CreateCategoryStore interface {
	Create(ctx context.Context, data *foodmodel.FoodCreate) error
}

type createCategoryBiz struct {
	store CreateCategoryStore
}

func NewCreateCategoryBiz(store CreateCategoryStore) *createCategoryBiz {
	return &createCategoryBiz{
		store: store,
	}
}

func (biz *createCategoryBiz) Create(ctx context.Context, data *categorymodel.CategoryCreate) error {
	err := biz.Create(ctx, data)
	if err != nil {
		return common.ErrCannotCreateEntity(categorymodel.EntityName, err)
	}

	return nil
}
