package categorybusiness

import (
	"context"
	"g06-food-delivery/common"
	categorymodel "g06-food-delivery/modules/category/model"
)

type ListCategoryStore interface {
	ListCategoryWithCondition(ctx context.Context,
		cond map[string]interface{},
		paging *common.Paging) ([]categorymodel.Category, error)
}

type listCategoryBiz struct {
	store ListCategoryStore
}

func NewListCategoryBiz(store ListCategoryStore) *listCategoryBiz {
	return &listCategoryBiz{store: store}
}

func (biz *listCategoryBiz) ListCategoryWithCondition(ctx context.Context,
	cond map[string]interface{},
	paging *common.Paging) ([]categorymodel.Category, error) {

	result, err := biz.store.ListCategoryWithCondition(ctx, cond, paging)
	if err != nil {
		return nil, common.ErrCannotListEntity(categorymodel.EntityName, err)
	}

	return result, nil
}
