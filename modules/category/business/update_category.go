package categorybusiness

import (
	"context"
	"g06-food-delivery/common"
	categorymodel "g06-food-delivery/modules/category/model"
)

type UpdateCategoryStore interface {
	Update(ctx context.Context, id int, data *categorymodel.CategoryUpdate) error
}

type updateCategoryBiz struct {
	store UpdateCategoryStore
}

func NewUpdateCategoryBiz(store UpdateCategoryStore) *updateCategoryBiz {
	return &updateCategoryBiz{store: store}
}

func (biz *updateCategoryBiz) Update(ctx context.Context, id int, data *categorymodel.CategoryUpdate) error {
	err := biz.store.Update(ctx, id, data)
	if err != nil {
		return common.ErrCannotUpdateEntity(categorymodel.EntityName, err)
	}

	return nil
}
