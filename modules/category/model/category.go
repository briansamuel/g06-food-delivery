package categorymodel

import "g06-food-delivery/common"

const EntityName = "Category"

type Category struct {
	common.SQLModel
	Name        string       `json:"name" gorm:"column:name"`
	Description string       `json:"description" gorm:"column:description"`
	Icon        common.Image `json:"icon" gorm:"column:icon"`
}

func (Category) TableName() string { return "categories" }

func (data *Category) Mask(isOwnerOrAdmin bool) {
	data.GenUID(common.DbTypeCategory)

}
