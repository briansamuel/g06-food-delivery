package categorymodel

import "g06-food-delivery/common"

type CategoryCreate struct {
	common.SQLModel
	Name        string       `json:"name" gorm:"column:name"`
	Description string       `json:"description" gorm:"column:description"`
	Icon        common.Image `json:"icon" gorm:"column:icon"`
}

func (CategoryCreate) TableName() string { return "categories" }
