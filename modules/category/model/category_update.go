package categorymodel

import "g06-food-delivery/common"

type CategoryUpdate struct {
	Status      int          `json:"status" gorm:"column:status"`
	Name        string       `json:"name" gorm:"column:name"`
	Description string       `json:"description" gorm:"column:description"`
	Icon        common.Image `json:"icon" gorm:"column:icon"`
}

func (CategoryUpdate) TableName() string { return "categories" }
