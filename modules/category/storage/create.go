package categorystorage

import (
	"context"
	"g06-food-delivery/common"
	categorymodel "g06-food-delivery/modules/category/model"
)

func (s *sqlStore) Create(ctx context.Context, data *categorymodel.CategoryCreate) error {
	db := s.db

	if err := db.Create(data).Error; err != nil {
		return common.ErrDB(err)
	}
	//Update
	return nil
}
