package categorystorage

import (
	"context"
	"g06-food-delivery/common"
	categorymodel "g06-food-delivery/modules/category/model"
	"gorm.io/gorm"
)

func (s *sqlStore) GetDataWithCondition(ctx context.Context,
	cond map[string]interface{}) (*categorymodel.Category, error) {

	var data categorymodel.Category
	if err := s.db.
		Where(cond).
		First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &data, nil
}
