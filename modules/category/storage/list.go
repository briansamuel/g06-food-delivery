package categorystorage

import (
	"context"
	"g06-food-delivery/common"
	categorymodel "g06-food-delivery/modules/category/model"
)

func (s *sqlStore) ListCategoryWithCondition(ctx context.Context,
	cond map[string]interface{},
	paging *common.Paging) ([]categorymodel.Category, error) {
	db := s.db
	var result []categorymodel.Category

	db = db.Where("status in (?)", 1)

	db = db.Where(cond)
	if err := db.Table(categorymodel.Category{}.TableName()).Count(&paging.Total).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	if err := db.
		Limit(paging.Limit).
		Offset((paging.Page - 1) * paging.Limit).
		Order("id desc").
		Find(&result).Error; err != nil {

		return nil, common.ErrDB(err)
	}

	return result, nil
}
