package foodbusiness

import (
	"context"
	"g06-food-delivery/common"
	foodmodel "g06-food-delivery/modules/food/model"
)

type CreateFoodStore interface {
	Create(ctx context.Context, data *foodmodel.FoodCreate) error
}

type createFoodBiz struct {
	store CreateFoodStore
}

func NewCreateFoodBiz(store CreateFoodStore) *createFoodBiz {
	return &createFoodBiz{store: store}
}

func (biz *createFoodBiz) CreateFood(ctx context.Context, data *foodmodel.FoodCreate) error {

	err := biz.store.Create(ctx, data)
	if err != nil {
		return common.ErrCannotCreateEntity(foodmodel.EntityName, err)
	}

	return nil
}
