package foodbusiness

import (
	"context"
	"g06-food-delivery/common"
	foodmodel "g06-food-delivery/modules/food/model"
)

type DeleteFoodStore interface {
	Delete(ctx context.Context, id int) error
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*foodmodel.Food, error)
	Update(ctx context.Context, id int, data *foodmodel.FoodUpdate) error
}

type deleteFoodBiz struct {
	store DeleteFoodStore
}

func NewDeleteFoodBiz(store DeleteFoodStore) *deleteFoodBiz {
	return &deleteFoodBiz{store: store}
}

func (biz *deleteFoodBiz) DeleteFood(ctx context.Context, id int, isSoft bool) error {
	oldData, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id})

	if err != nil {

		return err
	}
	if oldData.Status == 0 {
		return common.ErrEntityDeleted(foodmodel.EntityName, err)
	}
	if isSoft {
		zero := 0
		if err := biz.store.Update(ctx, id, &foodmodel.FoodUpdate{Status: &zero}); err != nil {

			return common.ErrCannotDeleteEntity(foodmodel.EntityName, err)
		}
		return nil
	}

	if err := biz.store.Delete(ctx, id); err != nil {

		return err
	}
	return nil
}
