package foodbusiness

import (
	"context"
	"g06-food-delivery/common"
	foodmodel "g06-food-delivery/modules/food/model"
)

type UpdateFoodStore interface {
	Update(ctx context.Context, id int, data *foodmodel.FoodUpdate) error
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*foodmodel.Food, error)
}

type updateFoodBiz struct {
	store UpdateFoodStore
}

func NewUpdateFoodBiz(store UpdateFoodStore) *updateFoodBiz {
	return &updateFoodBiz{store: store}
}

func (biz *updateFoodBiz) UpdateFood(ctx context.Context, id int, data *foodmodel.FoodUpdate) error {
	oldData, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id})

	if err != nil {

		return err
	}
	if oldData.Status == 0 {
		return common.ErrEntityDeleted(foodmodel.EntityName, err)
	}
	if err := biz.store.Update(ctx, id, data); err != nil {

		return err
	}
	return nil

}