package foodmodel

type FoodFilter struct {
	RestaurantId int `json:"restaurant_id" gorm:"column:restaurant_id"`
	CategoryId   int `json:"category_id" gorm:"column:category_id"`
	Price        int `json:"price" gorm:"column:price"`
}

func (FoodFilter) TableName() string { return "foods" }
