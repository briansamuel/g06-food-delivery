package foodstorage

import (
	"context"
	"g06-food-delivery/common"
	foodmodel "g06-food-delivery/modules/food/model"
)

func (s *sqlStore) Create(ctx context.Context, data *foodmodel.FoodCreate) error {
	db := s.db

	if err := db.Create(data).Error; err != nil {
		return common.ErrDB(err)
	}
	//Update
	return nil
}
