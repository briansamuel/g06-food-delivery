package foodstorage

import (
	"context"
	"g06-food-delivery/common"
	foodmodel "g06-food-delivery/modules/food/model"
)

func (s *sqlStore) ListFoodInRestaurant(ctx context.Context,
	filter *foodmodel.FoodFilter,
	paging *common.Paging) ([]foodmodel.Food, error) {
	db := s.db
	var result []foodmodel.Food

	db = db.Where("status in (?)", 1)

	if filter.RestaurantId > 0 {
		db = db.Where("restaurant = ?", filter.RestaurantId)
	}
	if err := db.Table(foodmodel.Food{}.TableName()).Count(&paging.Total).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	if err := db.
		Limit(paging.Limit).
		Offset((paging.Page - 1) * paging.Limit).
		Order("id desc").
		Find(&result).Error; err != nil {

		return nil, common.ErrDB(err)
	}

	return result, nil
}
