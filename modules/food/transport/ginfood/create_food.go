package ginfood

import (
	"g06-food-delivery/common"
	"g06-food-delivery/component/appctx"
	foodbusiness "g06-food-delivery/modules/food/business"
	foodmodel "g06-food-delivery/modules/food/model"
	foodstorage "g06-food-delivery/modules/food/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreateFood(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var newFood foodmodel.FoodCreate

		if err := c.ShouldBind(&newFood); err != nil {
			panic(common.ErrInternal(err))
		}
		//requester := c.MustGet(common.CurrentUser).(common.Requester)

		store := foodstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := foodbusiness.NewCreateFoodBiz(store)

		if err := biz.CreateFood(c.Request.Context(), &newFood); err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(newFood.ID))
	}
}
