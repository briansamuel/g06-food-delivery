package ginfood

import (
	"g06-food-delivery/common"
	"g06-food-delivery/component/appctx"
	foodbusiness "g06-food-delivery/modules/food/business"
	foodmodel "g06-food-delivery/modules/food/model"
	foodstorage "g06-food-delivery/modules/food/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func ListFood(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var filter foodmodel.FoodFilter
		var paging common.Paging
		paging.Process()
		if err := c.ShouldBind(&filter); err != nil {
			panic(common.ErrInternal(err))
		}

		store := foodstorage.NewSQLStore(appContext.GetMainDBConnection())

		biz := foodbusiness.NewListFoodBiz(store)

		result, err := biz.ListFoodInRestaurant(c.Request.Context(), &filter, &paging)
		if err != nil {
			panic(err)
		}

		for i := range result {
			result[i].Mask(false)
		}
		c.JSON(http.StatusOK, common.NewSuccessResponse(result, paging, filter))
	}
}
