package restaurantmodel

import "g06-food-delivery/common"

type Filter struct {
	common.SQLModel
	Name    string `json:"name" gorm:"column:name"`
	Address string `json:"address" gorm:"column:addr"`
	OwnerId int    `json:"owner_id" gorm:"column:owner_id"`
}

func (Filter) TableName() string { return Restaurant{}.TableName() }
