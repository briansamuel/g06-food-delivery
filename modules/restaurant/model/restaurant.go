package restaurantmodel

import (
	"g06-food-delivery/common"
)

const EntityName = "Restaurant"

type Restaurant struct {
	common.SQLModel
	Name      string             `gorm:"column:name"`
	Address   string             `gorm:"column:addr"`
	OwnerId   int                `json:"owner_id" gorm:"column:owner_id"`
	Logo      common.Image       `json:"logo" gorm:"logo"`
	Cover     common.Images      `json:"cover" gorm:"cover"`
	User      *common.SimpleUser `json:"user" gorm:"foreignKey:OwnerId;preload:false;"`
	LikeCount int                `json:"like_count" gorm:"like_count"`
}

func (Restaurant) TableName() string { return "restaurants" }

func (data *Restaurant) Mask(isOwnerOrAdmin bool) {
	data.GenUID(common.DbTypeRestaurant)

	if u := data.User; u != nil {
		u.Mask(isOwnerOrAdmin)
	}
}
