package restaurantstore

import (
	"context"
	"g06-food-delivery/common"
	restaurantmodel "g06-food-delivery/modules/restaurant/model"
)

func (s *sqlStore) Create(ctx context.Context, data *restaurantmodel.RestaurantCreate) error {
	db := s.db

	if err := db.Create(data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
