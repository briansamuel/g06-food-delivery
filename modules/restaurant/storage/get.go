package restaurantstore

import (
	"context"
	"g06-food-delivery/common"
	"gorm.io/gorm"

	restaurantmodel "g06-food-delivery/modules/restaurant/model"
)

func (s *sqlStore) GetDataWithCondition(ctx context.Context,
	cond map[string]interface{}) (*restaurantmodel.Restaurant, error) {

	var data restaurantmodel.Restaurant
	if err := s.db.
		Where(cond).
		First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &data, nil
}
