package ginrestaurant

import (
	"g06-food-delivery/common"
	"g06-food-delivery/component/appctx"
	bizrestaurant "g06-food-delivery/modules/restaurant/business"
	restaurantmodel "g06-food-delivery/modules/restaurant/model"
	restaurantstore "g06-food-delivery/modules/restaurant/storage"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateRestaurant(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var newRestaurant restaurantmodel.RestaurantCreate

		if err := c.ShouldBind(&newRestaurant); err != nil {
			panic(common.ErrInternal(err))
		}
		requester := c.MustGet(common.CurrentUser).(common.Requester)

		newRestaurant.OwnerId = requester.GetUserId()
		store := restaurantstore.NewSQLStore(appContext.GetMainDBConnection())
		biz := bizrestaurant.NewCreateRestaurantBiz(store)

		if err := biz.CreateRestaurant(c.Request.Context(), &newRestaurant); err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(newRestaurant.ID))
	}
}
