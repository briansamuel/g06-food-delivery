package restaurantlikebiz

import (
	"context"
	"g06-food-delivery/common"
	restaurantlikemodel "g06-food-delivery/modules/restaurantlike/model"
	"g06-food-delivery/pubsub"
)

type UserDislikeRestaurantStorage interface {
	Delete(ctx context.Context, data *restaurantlikemodel.Like) error
}

//type DecreaseLikeStore interface {
//	DecreaseLikeCount(ctx context.Context, id int) error
//}

type userDislikeRestaurantBiz struct {
	store UserDislikeRestaurantStorage
	//storeDecrease DecreaseLikeStore
	pb pubsub.PubSub
}

func NewUserDislikeRestaurantBiz(
	store UserDislikeRestaurantStorage,
//storeDecrease DecreaseLikeStore,
	pb pubsub.PubSub,
) *userDislikeRestaurantBiz {
	return &userDislikeRestaurantBiz{
		store: store,
		//storeDecrease: storeDecrease,
		pb: pb,
	}
}

func (biz *userDislikeRestaurantBiz) DislikeRestaurant(ctx context.Context, data *restaurantlikemodel.Like) error {
	err := biz.store.Delete(ctx, data)

	if err != nil {
		return restaurantlikemodel.ErrCannotLikeRestaurant(err)
	}

	// Use Async Job Method to increase like
	//go func() {
	//	defer common.Recover()
	//
	//	job := asyncjob.NewJob(func(ctx context.Context) error {
	//		return biz.storeDecrease.DecreaseLikeCount(ctx, data.RestaurantID)
	//	})
	//
	//	job.SetTryDurations([]time.Duration{time.Second * 10, time.Second * 20, time.Second * 30})
	//
	//	_ = asyncjob.NewGroup(true, job).Run(ctx)
	//}()

	// Use PubSub to increase like
	go func() {
		defer common.Recover()
		_ = biz.pb.Publish(ctx, common.TopicUserDislikeRestaurant, pubsub.NewMessage(data))
	}()

	return nil
}
