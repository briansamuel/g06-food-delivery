package restaurantlikebiz

import (
	"context"
	"g06-food-delivery/common"
	restaurantlikemodel "g06-food-delivery/modules/restaurantlike/model"
	"g06-food-delivery/pubsub"
)

type UserLikeRestaurantStorage interface {
	Create(ctx context.Context, data *restaurantlikemodel.Like) error
}

//type IncreaseLikeStore interface {
//	IncreaseLikeCount(ctx context.Context, id int) error
//}

type userLikeRestaurantBiz struct {
	store UserLikeRestaurantStorage
	//storeIncrease IncreaseLikeStore
	pb pubsub.PubSub
}

func NewUserLikeRestaurantBiz(
	store UserLikeRestaurantStorage,
//storeIncrease IncreaseLikeStore,
	pb pubsub.PubSub,
) *userLikeRestaurantBiz {
	return &userLikeRestaurantBiz{
		store: store,
		//storeIncrease: storeIncrease,
		pb: pb,
	}
}

func (biz *userLikeRestaurantBiz) LikeRestaurant(ctx context.Context, data *restaurantlikemodel.Like) error {
	err := biz.store.Create(ctx, data)

	if err != nil {
		return restaurantlikemodel.ErrCannotLikeRestaurant(err)
	}

	// Use Async Job Method to increase like
	//go func() {
	//	defer common.Recover()
	//
	//	job := asyncjob.NewJob(func(ctx context.Context) error {
	//		return biz.storeIncrease.IncreaseLikeCount(ctx, data.RestaurantID)
	//	})
	//
	//	job.SetTryDurations([]time.Duration{time.Second * 10, time.Second * 20, time.Second * 30})
	//
	//	_ = asyncjob.NewGroup(true, job).Run(ctx)
	//}()

	// Use PubSub to increase like
	go func() {
		defer common.Recover()
		_ = biz.pb.Publish(ctx, common.TopicUserLikeRestaurant, pubsub.NewMessage(data))
	}()
	return nil
}
