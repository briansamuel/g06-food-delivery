package restaurantlikebiz

import (
	"context"
	"g06-food-delivery/common"
	restaurantlikemodel "g06-food-delivery/modules/restaurantlike/model"
)

type UsersLikeRestaurantStorage interface {
	GetUsersLikeRestaurant(ctx context.Context,
		conditions map[string]interface{},
		filter *restaurantlikemodel.Filter,
		paging *common.Paging,
		moreKeys ...string,
	) ([]restaurantlikemodel.User, error)
}

type usersLikeRestaurantBiz struct {
	store UsersLikeRestaurantStorage
}

func NewUsersLikeRestaurantBiz(store UsersLikeRestaurantStorage) *usersLikeRestaurantBiz {
	return &usersLikeRestaurantBiz{store: store}
}

func (biz *usersLikeRestaurantBiz) ListUserLikeRestaurant(ctx context.Context,
	filter *restaurantlikemodel.Filter,
	paging *common.Paging,
	moreKeys ...string) ([]restaurantlikemodel.User, error) {

	result, err := biz.store.GetUsersLikeRestaurant(ctx, nil, filter, paging)
	if err != nil {
		return nil, common.ErrCannotListEntity(restaurantlikemodel.EntityName, err)
	}

	return result, nil
}
