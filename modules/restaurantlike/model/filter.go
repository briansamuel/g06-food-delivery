package restaurantlikemodel

type Filter struct {
	RestaurantID int `json:"restaurant_id" gorm:"column:restaurant_id"`
	UserId       int `json:"user_id" gorm:"column:user_id"`
}

func (data Filter) TableName() string { return "restaurant_likes" }
