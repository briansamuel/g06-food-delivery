package restaurantlikegin

import (
	"g06-food-delivery/common"
	"g06-food-delivery/component/appctx"
	userlikerestaurantbiz "g06-food-delivery/modules/restaurantlike/biz"
	restaurantlikemodel "g06-food-delivery/modules/restaurantlike/model"
	restaurantlikestorage "g06-food-delivery/modules/restaurantlike/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Like(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		db := appContext.GetMainDBConnection()
		uid, err := common.FromBase58(c.Param("id"))
		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)

		data := restaurantlikemodel.Like{
			RestaurantID: int(uid.GetLocalID()),
			UserId:       requester.GetUserId(),
		}
		store := restaurantlikestorage.NewSQLStore(db)
		//countStore := restaurantstore.NewSQLStore(db)
		pb := appContext.GetPubSub()
		biz := userlikerestaurantbiz.NewUserLikeRestaurantBiz(store, pb)

		if err := biz.LikeRestaurant(c.Request.Context(), &data); err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(true))
	}
}
