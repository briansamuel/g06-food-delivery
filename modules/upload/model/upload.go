package uploadmodel

import (
	"fmt"
	"g06-food-delivery/common"
)

func ErrFileIsNotImage(err error) *common.AppError {
	return common.NewCustomError(err, fmt.Sprint("file is not image"), fmt.Sprint("ErrFileIsNotImage"))
}

func ErrCannotSaveFile(err error) *common.AppError {
	return common.NewCustomError(err, fmt.Sprint("cannot save file"), fmt.Sprint("ErrCannotSaveFile"))
}
