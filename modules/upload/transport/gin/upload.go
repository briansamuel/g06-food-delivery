package ginupload

import (
	"g06-food-delivery/common"
	"g06-food-delivery/component/appctx"
	uploadbusiness "g06-food-delivery/modules/upload/business"
	"github.com/gin-gonic/gin"
)

func Upload(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		fileHanle, err := c.FormFile("file")
		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		folder := c.DefaultPostForm("folder", "img")

		file, err := fileHanle.Open()

		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		defer file.Close()

		dataBytes := make([]byte, fileHanle.Size)
		if _, err := file.Read(dataBytes); err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		biz := uploadbusiness.NewUploadBiz(appContext.UploadProvider())
		img, err := biz.Upload(c.Request.Context(), dataBytes, folder, fileHanle.Filename)
		//if err := c.SaveUploadedFile(fileHanle, fmt.Sprintf("static/%s", fileHanle.Filename)); err != nil {
		//	panic(common.ErrInternal(err))
		//}

		if err != nil {
			panic(err)
		}
		c.JSON(200, common.SimpleSuccessResponse(img))
	}
}
