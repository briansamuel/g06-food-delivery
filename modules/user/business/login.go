package userbiz

import (
	"context"
	"g06-food-delivery/common"
	"g06-food-delivery/component/tokenprovider"
	usermodel "g06-food-delivery/modules/user/model"
)

type LoginStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}, moreInfo ...string) (*usermodel.User, error)
}

type loginBiz struct {
	loginStore    LoginStore
	tokenProvider tokenprovider.Provider
	hasher        Hasher
	expiry        int
}

func NewLoginBiz(loginStore LoginStore, tokenProvider tokenprovider.Provider, hasher Hasher,
	expiry int) *loginBiz {
	return &loginBiz{
		loginStore:    loginStore,
		tokenProvider: tokenProvider,
		hasher:        hasher,
		expiry:        expiry,
	}
}

func (biz *loginBiz) Login(ctx context.Context, data *usermodel.UserLogin) (*tokenprovider.Token, error) {

	if err := data.Validate(); err != nil {
		return nil, err
	}
	user, err := biz.loginStore.FindUser(ctx, map[string]interface{}{"email": data.Email})

	if err != nil {
		return nil, usermodel.ErrEmailOrPasswordInvalid
	}

	passHashed := biz.hasher.Hash(data.Password + user.Salt)

	if user.Password != passHashed {
		return nil, usermodel.ErrEmailOrPasswordInvalid
	}

	payload := tokenprovider.TokenPayload{
		UserId: user.ID,
		Role:   user.Role,
	}

	accessToken, err := biz.tokenProvider.Generate(payload, biz.expiry)
	if err != nil {
		return nil, common.ErrInternal(err)

	}

	return accessToken, nil
}
