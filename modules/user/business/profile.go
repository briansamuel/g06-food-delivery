package userbiz

import (
	"context"
	"g06-food-delivery/common"
	usermodel "g06-food-delivery/modules/user/model"
)

type ProfileStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}, moreInfo ...string) (*usermodel.User, error)
}

type profileBiz struct {
	profileStore ProfileStore
}

func NewProfileBiz(profileStore ProfileStore) *profileBiz {
	return &profileBiz{profileStore: profileStore}
}

func (biz *profileBiz) Profile(ctx context.Context, id int) (*usermodel.User, error) {
	user, err := biz.profileStore.FindUser(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return nil, common.ErrEntityNotFound(usermodel.EntityName, err)
	}

	user.Mask(false)
	return user, nil
}
