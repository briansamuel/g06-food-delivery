package model

import (
	"g06-food-delivery/common"
)

const EntityName = "User"

type User struct {
	common.SQLModel `json:",inline"`
	Email           string       `json:"email" gorm:"column:email;"`
	Password        string       `json:"-" gorm:"column:password;"`
	Salt            string       `json:"-" gorm:"column:salt"`
	LastName        string       `json:"last_name" gorm:"column:last_name;"`
	FirstName       string       `json:"first_name" gorm:"column:first_name;"`
	Role            string       `json:"role" gorm:"column:role;"`
	Avatar          common.Image `json:"avatar" gorm:"avatar"`
}

func (User) TableName() string { return "users" }

func (u *User) Mask(isOwnerOrAdmin bool) {
	u.GenUID(common.DbTypeUser)
}

func (u *User) GetUserId() int {
	return u.ID
}

func (u *User) GetEmail() string {
	return u.Email
}

func (u *User) GetRole() string {
	return u.Role
}

type UserCreate struct {
	common.SQLModel `json:",inline"`
	Email           string        `json:"email" gorm:"column:email;"`
	Password        string        `json:"password" gorm:"column:password;"`
	Salt            string        `json:"salt" gorm:"column:salt"`
	LastName        string        `json:"last_name" gorm:"column:last_name;"`
	FirstName       string        `json:"first_name" gorm:"column:first_name;"`
	Role            string        `json:"role" gorm:"column:role;"`
	Avatar          *common.Image `json:"avatar" gorm:"avatar"`
}

func (UserCreate) TableName() string { return User{}.TableName() }

func (u *UserCreate) Mask(isOwnerOrAdmin bool) {
	u.GenUID(common.DbTypeUser)
}

type UserLogin struct {
	Email    string `json:"email" gorm:"column:email;"`
	Password string `json:"password" gorm:"column:password;"`
}

func (UserLogin) TableName() string { return User{}.TableName() }

type UserUpdate struct {
	common.SQLModel `json:",inline"`
	Password        string        `json:"password" gorm:"column:password;"`
	Salt            string        `json:"salt" gorm:"column:salt"`
	LastName        string        `json:"last_name" gorm:"column:last_name;"`
	FirstName       string        `json:"first_name" gorm:"column:first_name;"`
	Avatar          *common.Image `json:"avatar" gorm:"column:avatar"`
	Lat             float64       `json:"lat" gorm:"column:lat"`
	Lng             float64       `json:"lng" gorm:"column:lng"`
}

func (UserUpdate) TableName() string { return User{}.TableName() }

type UserProfile struct {
	common.SQLModel `json:",inline"`
	LastName        string        `json:"last_name" gorm:"column:last_name;"`
	FirstName       string        `json:"first_name" gorm:"column:first_name;"`
	Avatar          *common.Image `json:"avatar" gorm:"avatar"`
}

func (UserUpdate) UserProfile() string { return User{}.TableName() }
