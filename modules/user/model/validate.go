package model

import "net/mail"

func (data *UserCreate) Validate() error {
	_, err := mail.ParseAddress(data.Email)
	if err != nil {
		return ErrEmailValidateInvalid
	}

	if len(data.Password) < 6 {
		return ErrPasswordWeak
	}
	return nil
}

func (data *UserLogin) Validate() error {
	_, err := mail.ParseAddress(data.Email)
	if err != nil {
		return ErrEmailValidateInvalid
	}

	if len(data.Password) < 6 {
		return ErrPasswordWeak
	}
	
	return nil
}
