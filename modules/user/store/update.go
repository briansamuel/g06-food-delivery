package userstore

import (
	"context"
	"g06-food-delivery/common"
	usermodel "g06-food-delivery/modules/user/model"
)

func (s *sqlStore) Update(ctx context.Context, id int, data *usermodel.UserUpdate) error {
	db := s.db

	if err := db.Where("id = ?", id).Updates(&data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
