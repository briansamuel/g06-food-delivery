package ginuser

import (
	"g06-food-delivery/common"
	"g06-food-delivery/component/appctx"
	"g06-food-delivery/component/hasher"
	"g06-food-delivery/component/tokenprovider/jwt"
	userbiz "g06-food-delivery/modules/user/business"
	usermodel "g06-food-delivery/modules/user/model"
	userstore "g06-food-delivery/modules/user/store"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Login(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var loginUserData usermodel.UserLogin

		if err := c.ShouldBind(&loginUserData); err != nil {
			panic(common.ErrInvalidRequest(err))
		}
		db := appContext.GetMainDBConnection()
		tokenProvider := jwt.NewTokenJWTProvider(appContext.SecretKey())
		store := userstore.NewSQLStore(db)
		md5 := hasher.NewMd5Hash()
		loginBiz := userbiz.NewLoginBiz(store, tokenProvider, md5, 60*60*24*30)

		account, err := loginBiz.Login(c.Request.Context(), &loginUserData)
		if err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(account))
	}
}
