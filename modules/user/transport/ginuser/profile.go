package ginuser

import (
	"g06-food-delivery/common"
	"g06-food-delivery/component/appctx"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Profile(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {

		u := c.MustGet(common.CurrentUser)

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(u))
	}
}
