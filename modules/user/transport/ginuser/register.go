package ginuser

import (
	"g06-food-delivery/common"
	"g06-food-delivery/component/appctx"
	"g06-food-delivery/component/hasher"
	userbiz "g06-food-delivery/modules/user/business"
	usermodel "g06-food-delivery/modules/user/model"
	userstore "g06-food-delivery/modules/user/store"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Register(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		db := appContext.GetMainDBConnection()
		var data usermodel.UserCreate

		if err := c.ShouldBind(&data); err != nil {
			panic(common.ErrInternal(err))
		}
		store := userstore.NewSQLStore(db)
		md5 := hasher.NewMd5Hash()
		registerBiz := userbiz.NewRegisterBiz(store, md5)

		if err := registerBiz.Register(c.Request.Context(), &data); err != nil {
			panic(err)
		}
		data.Mask(false)
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data.FakeId.String()))
	}
}
