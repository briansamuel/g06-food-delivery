package skiouser

import (
	"context"
	"fmt"
	"g06-food-delivery/common"
	usermodel "g06-food-delivery/modules/user/model"
	userstore "g06-food-delivery/modules/user/store"
	socketio "github.com/googollee/go-socket.io"
	"gorm.io/gorm"
	"log"
)

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	GetUser(conn socketio.Conn) common.Requester
}

type UserLocation struct {
	Lat float64
	Lng float64
}

func (ul UserLocation) String() string {
	return fmt.Sprintf("Lat: %f, Lng: %f \n", ul.Lat, ul.Lng)
}

func RealtimeUserUploadLocation(appCtx AppContext) func(socketio.Conn, UserLocation) {
	return func(conn socketio.Conn, loc UserLocation) {
		log.Println("RealtimeUserUploadLocation", loc.String())
		log.Println("RealtimeUserUploadLocation", loc.String(), ", requester:", appCtx.GetUser(conn).GetUserId())
		var data usermodel.UserUpdate
		data.Lat = loc.Lat
		data.Lng = loc.Lng
		store := userstore.NewSQLStore(appCtx.GetMainDBConnection())
		store.Update(context.Background(), appCtx.GetUser(conn).GetUserId(), &data)

	}

}
