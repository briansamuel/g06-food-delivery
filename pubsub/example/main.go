package main

import (
	"context"
	"g06-food-delivery/pubsub"
	"g06-food-delivery/pubsub/localpb"
	"log"
	"time"
)

func main() {
	var localps pubsub.PubSub = localpb.NewPubSub()

	var topic pubsub.Topic = "OrderCreated"

	sub1, close1 := localps.Subscribe(context.Background(), topic)
	sub2, _ := localps.Subscribe(context.Background(), topic)

	localps.Publish(context.Background(), topic, pubsub.NewMessage(2))
	//localps.Publish(context.Background(), topic, pubsub.NewMessage(2))

	go func() {
		for {
			log.Println("Sub 1: ", (<-sub1).Data())
			time.Sleep(time.Millisecond * 400)
		}
	}()

	go func() {
		for {
			log.Println("Sub 2: ", (<-sub2).Data())
			time.Sleep(time.Millisecond * 400)
		}
	}()
	time.Sleep(time.Second * 3)
	close1()

	localps.Publish(context.Background(), topic, pubsub.NewMessage(3))

	time.Sleep(time.Second * 2)
}
