package localpb

import (
	"context"
	"g06-food-delivery/pubsub"
	"log"
	"sync"
)

type localPubSub struct {
	messageQueue chan *pubsub.Message
	mapChanel    map[pubsub.Topic][]chan *pubsub.Message
	locker       *sync.RWMutex
}

func NewPubSub() *localPubSub {
	pb := &localPubSub{
		messageQueue: make(chan *pubsub.Message, 10000),
		mapChanel:    make(map[pubsub.Topic][]chan *pubsub.Message),
		locker:       new(sync.RWMutex),
	}
	pb.run()
	return pb
}

func (ps *localPubSub) Publish(ctx context.Context, topic pubsub.Topic, data *pubsub.Message) error {
	data.SetChanel(topic)

	//go func() {
	//	defer common.Recover()
	//	ps.messageQueue <- data
	//	log.Println("New event published:", data.String(), "with", data.Data())
	//}()

	ps.messageQueue <- data
	log.Println("New event published:", data.String(), "with", data.Data())

	return nil
}

func (ps *localPubSub) Subscribe(ctx context.Context, topic pubsub.Topic) (ch <-chan *pubsub.Message, close func()) {
	c := make(chan *pubsub.Message)

	ps.locker.Lock()

	if val, ok := ps.mapChanel[topic]; ok {
		val = append(ps.mapChanel[topic], c)
		ps.mapChanel[topic] = val
	} else {
		ps.mapChanel[topic] = []chan *pubsub.Message{c}
	}

	ps.locker.Unlock()

	return c, func() {
		log.Println("Unsubscribe: ")

		// Get chans in mapChanel with Topic
		if chans, ok := ps.mapChanel[topic]; ok {
			// Loop chans
			for i := range chans {
				// If chans[i] == c, remove it
				if chans[i] == c {
					chans = append(chans[:i], chans[i+1:]...)

					ps.locker.Lock()
					// Set mapChanel with new chans after remove
					ps.mapChanel[topic] = chans
					ps.locker.Unlock()
					break
				}
			}
		}
	}
}

func (ps *localPubSub) run() error {
	log.Println("PubSub started")

	go func() {
		//defer common.Recover()
		for {
			mess := <-ps.messageQueue // Get message from queue

			log.Println("Message dequeue", mess.String())

			if subs, ok := ps.mapChanel[mess.Chanel()]; ok {
				for i := range subs {
					go func(c chan *pubsub.Message) {
						//defer common.Recover()
						c <- mess
					}(subs[i])
				}
			}
		}
	}()

	return nil
}
