package pubsub

import (
	"fmt"
	"time"
)

type Message struct {
	id       string
	chanel   Topic
	data     interface{}
	createAt time.Time
}

func NewMessage(data interface{}) *Message {

	now := time.Now().UTC()
	return &Message{
		id:       fmt.Sprintf("%d", time.Now().UnixNano()),
		data:     data,
		createAt: now,
	}
}

func (evt *Message) String() string {
	return fmt.Sprintf("Message %s", evt.chanel)
}

func (evt *Message) Chanel() Topic {
	return evt.chanel
}

func (evt *Message) SetChanel(chanel Topic) {
	evt.chanel = chanel
}

func (evt *Message) Data() interface{} {
	return evt.data
}
