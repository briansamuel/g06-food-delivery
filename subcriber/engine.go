package subscriber

import (
	"context"
	"g06-food-delivery/component/appctx"
	"g06-food-delivery/component/asyncjob"
	"g06-food-delivery/pubsub"
	"log"
)

type consumerJob struct {
	Title string
	Hdl   func(ctx context.Context, msg *pubsub.Message) error
}

type subscriber struct {
	appCtx appctx.AppContext
}

func NewSubscriber(appContext appctx.AppContext) *subscriber {
	return &subscriber{
		appCtx: appContext,
	}
}

func (sb *subscriber) Start() error {

	// Follow topic in queue
	sb.Setup()

	return nil
}

type GroupJob interface {
	Run(ctx context.Context) error
}

func (sb *subscriber) startSubTopic(topic pubsub.Topic, isConcurrency bool, consumerJobs ...consumerJob) {
	c, _ := sb.appCtx.GetPubSub().Subscribe(context.Background(), topic)

	for _, item := range consumerJobs {
		log.Println("Setup subscriber for:", item.Title)
	}

	getJobHandler := func(job *consumerJob, msg *pubsub.Message) asyncjob.JobHandler {
		return func(ctx context.Context) error {
			log.Println("running for job", job.Title, ". Value", msg.Data())
			return job.Hdl(ctx, msg)
		}
	}

	go func() {
		for {
			msg := <-c
			jobHdlArr := make([]asyncjob.Job, len(consumerJobs))

			for i := range consumerJobs {
				jobHdl := getJobHandler(&consumerJobs[i], msg)

				jobHdlArr[i] = asyncjob.NewJob(jobHdl, asyncjob.WithName(consumerJobs[i].Title))

			}

			groups := asyncjob.NewGroup(isConcurrency, jobHdlArr...)

			if err := groups.Run(context.Background()); err != nil {
				log.Println(err)
			}
		}
	}()

}
