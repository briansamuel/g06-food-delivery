package subscriber

import (
	"context"
	"g06-food-delivery/component/appctx"
	restaurantstore "g06-food-delivery/modules/restaurant/storage"
	restaurantlikemodel "g06-food-delivery/modules/restaurantlike/model"
	"g06-food-delivery/pubsub"
	"log"
)

// Pubsub without Engine
func RunIncreaseLikeCountAfterUserLikeRestaurant(appCtx appctx.AppContext) func(*pubsub.Message, context.Context) error {
	return func(msg *pubsub.Message, ctx context.Context) error {
		store := restaurantstore.NewSQLStore(appCtx.GetMainDBConnection())

		likeData := msg.Data().(*restaurantlikemodel.Like)

		return store.IncreaseLikeCount(ctx, likeData.RestaurantID)
	}
}

// Pubsub with Engine Subcribe
func DecreaseLikeCountAfterUserLikeRestaurant(appCtx appctx.AppContext) consumerJob {
	return consumerJob{
		Title: "Decrease like count after user dislike restaurant",
		Hdl: func(ctx context.Context, msg *pubsub.Message) error {
			store := restaurantstore.NewSQLStore(appCtx.GetMainDBConnection())
			likeData := msg.Data().(*restaurantlikemodel.Like)
			return store.DecreaseLikeCount(ctx, likeData.RestaurantID)
		},
	}
}

func IncreaseLikeCountAfterUserLikeRestaurant(appCtx appctx.AppContext) consumerJob {
	return consumerJob{
		Title: "Increase like count after user like restaurant",
		Hdl: func(ctx context.Context, msg *pubsub.Message) error {
			store := restaurantstore.NewSQLStore(appCtx.GetMainDBConnection())
			likeData := msg.Data().(*restaurantlikemodel.Like)
			return store.IncreaseLikeCount(ctx, likeData.RestaurantID)
		},
	}
}

func RealtimeLikeCountAfterUserLikeRestaurant(appCtx appctx.AppContext) consumerJob {
	return consumerJob{
		Title: "Realtime like count after user like restaurant",
		Hdl: func(ctx context.Context, msg *pubsub.Message) error {

			likeData := msg.Data().(*restaurantlikemodel.Like)
			return appCtx.GetRealtimeEngine().EmitToUser(likeData.GetUserId(), string(msg.Chanel()), likeData)
		},
	}
}

func PushNotificationWhenUserSubscribe(appCtx appctx.AppContext) consumerJob {
	return consumerJob{
		Title: "Push Notification when user subscribe",
		Hdl: func(ctx context.Context, msg *pubsub.Message) error {
			log.Println("Push Notification success")
			return nil
		},
	}
}
