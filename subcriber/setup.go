package subscriber

import "g06-food-delivery/common"

func (sb *subscriber) Setup() {

	// Follow topic in queue
	sb.startSubTopic(
		common.TopicUserLikeRestaurant,
		true,
		IncreaseLikeCountAfterUserLikeRestaurant(sb.appCtx),
		RealtimeLikeCountAfterUserLikeRestaurant(sb.appCtx),
		PushNotificationWhenUserSubscribe(sb.appCtx),
	)

	sb.startSubTopic(
		common.TopicUserDislikeRestaurant,
		true,
		DecreaseLikeCountAfterUserLikeRestaurant(sb.appCtx),
	)
}
